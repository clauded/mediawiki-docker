# Docker Compose file for the official MediaWiki image with extensions

## Installation

### Install Docker

Install Docker according to the [official instruction]
(https://docs.docker.com/install/linux/docker-ce/debian/#install-using-the-repository).

### Configure database usernames and password

The database usernames and passwords aren't stored in this repository,
so you'll have to create the file which contain them yourself. First
create the file `mysql_secrets.env` with the following adjusted to your environment:

```
MYSQL_ROOT_PASSWORD=some_root_password
MYSQL_DATABASE=my_wiki
MYSQL_USER=some_mysql_username
MYSQL_PASSWORD=some_mysql_password
# It's also possible to use a random generated password for root user
#MYSQL_RANDOM_ROOT_PASSWORD=yes
```

Then create the file `mediawiki_secrets.php` with the following, so it matches 
the contents of `mysql_secrets.env`.

```
<?php
$wgDBname = "my_wiki";
$wgDBuser = "mysql_username";
$wgDBpassword = "mysql_password";
```

To test your setup run these commands:

```
sudo rm -Rf images/ db/
docker-compose -f stack.yml up
```

Then point your browser at the IP address of the container (example):

```
http://172.19.0.4/mw-config
```

The host name of the docker database server is `database`. Other infos are
those specified in the file `mysql_secrets.env`.

After initial setup, download and replace `LocalSettings.php` in the root directory. 
Also uncomment the following line in `stack.yml`:

```
#- ./LocalSettings.php:/var/www/html/LocalSettings.php
```

Stop your stack (`CTRL+C`) then use `docker-compose -f stack.yml up` to restart 
the container.

### Create a MediaWiki image with the plugins you need

The file `mediawiki_extended/Dockerfile` adds the plugins you want to add to the 
[official MediaWiki docker image](https://hub.docker.com/_/mediawiki/). 
Turn this into a usable image using the following:

```
docker build -t name[:tag] mediawiki_extended
```

For example:

```
docker build -t mymediawiki:latest mediawiki_extended
```

### Start MediaWiki and the services it needs

The file `stack.yml` contains instructions for launching the
`name[:tag]` image created above along with the other containers 
it needs to work. Together these containers are called a "stack".

Start the stack using:

```
docker swarm init
docker stack deploy -c stack.yml my-mediawiki
```

Stop the stack using:

```
docker stack rm my-mediawiki
docker swarm leave --force
```

Note

This work is a derivative of https://github.com/divinenephron/docker-mediawiki